#![allow(dead_code)]
// https://www.codewars.com/kata/chess-fun-number-2-bishop-and-pawn

/// Takes a string of chess grid coodinates and returns the point on an x,y grid
///
/// The point a1 will be mapped to (0,0), function returns a tuple of i32s
fn grid_to_coord(grid: &str) -> (i32, i32) {
    let mut x = 0;
    let mut y = 0;

    for c in grid.chars() {
        match c {
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h': y = 0,
            1: x = 1
        }

    }

    (x, y)
}

#[test]
fn grid_to_coord_test() {
    assert_eq!(grid_to_coord("a1"), (0, 0));
    assert_eq!(grid_to_coord("a2"), (0, 1));
}

// fn bishop_and_pawn(bishop: &str, pawn: &str) -> bool {
//     return false;
// }

// #[test]
// fn basic_tests() {
//     // assert_eq!(bishop_and_pawn("a1", "c3"), true);
//     // assert_eq!(bishop_and_pawn("a1", "c3"), true);
//     // assert_eq!(bishop_and_pawn("h1", "h3"), false);
//     // assert_eq!(bishop_and_pawn("a5", "c3"), true);
//     // assert_eq!(bishop_and_pawn("g1", "f3"), false);
// }
